package com.FizzBuzz;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumber {

	public List<Integer> getPrimeNumbersInRange(int range) {
		List<Integer> list = new ArrayList<Integer>();
		checkAllNumbersGreaterOrEqual2IfArePrime(range, list);
		return list;
	}

	private void checkAllNumbersGreaterOrEqual2IfArePrime(int range, List<Integer> list) {
		for (int i = 2; i <= range; i++) {
			checkIfPrimeThenAddTolist(list, i, (int) Math.sqrt(i));
		}
	}

	private void checkIfPrimeThenAddTolist(List<Integer> list, int i, int max_divider) {
		if (!checkIfHasDividers(i, max_divider, false)) {
			list.add(i);
		}
	}

	private boolean checkIfHasDividers(int i, int max_divider, boolean isPrime) {
		for (int j = 2; j <= max_divider; j++) {
			if (checkIfNumberDivides(i, isPrime, j)) {
				break;
			}
		}
		return isPrime;
	}

	private boolean checkIfNumberDivides(int i, boolean isPrime, int j) {
		if (i % j == 0) {
			isPrime = true;
		}
		return isPrime;
	}
}
