package com.FizzBuzz;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class PrimeNumberTest {

	PrimeNumber primeNumbers;
	
	@Test
	@Parameters({ 
		"6"
	})
	public void checkPrimeNumbers( int range) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(2);
		list.add(3);
		list.add(5);
		assertEquals(list, primeNumbers.getPrimeNumbersInRange(range));
		/*assertThat(list)
	      .isEqualTo(refactor.getPrimeNumbers(range));*/
	}

	@Test
	@Parameters({ 
		"6, 2, 3, 5",
		"2, 2"
	})
	public void checkPrimeNumbers(String... args) {
		int range = Integer.parseInt(args[0]);
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 1; i < args.length; i++) {
			list.add(Integer.parseInt(args[i]));
		}
		assertThat(list)
	      .isEqualTo(primeNumbers.getPrimeNumbersInRange(range));
		

	}
	
	@Test
	@Parameters({ 
		"6, 2, 3, 5",
		"2, 2"
	})
	public void checkNumberOfPrimeNumbers(String... args) {
		int range = Integer.parseInt(args[0]);
		List<Integer> list = primeNumbers.getPrimeNumbersInRange(range);
		assertThat(list.size())
	      .isLessThanOrEqualTo(range);
	}
	

	@Test
	@Parameters({ 
		"6, 2, 3, 5",
		"2, 2"
	})
	public void checkIfMinPrimeNumbersIs2(String... args) {
		int range = Integer.parseInt(args[0]);
		List<Integer> list = primeNumbers.getPrimeNumbersInRange(range);
		assertThat(list.get(0))
	      .isEqualTo(2);
	}
	

	@Test
	@Parameters({ 
		"1"
	})
	public void checkIfMinPrimeNumbersIsNotReturnedIfRangeIsSmallerThan2(String... args) {
		int range = Integer.parseInt(args[0]);
		List<Integer> list = primeNumbers.getPrimeNumbersInRange(range);
		assertThat(list.size())
	      .isEqualTo(0);
	}
	
	@Test
	@Parameters({ 
		"6",
		"13"
	})
	public void checkIfMaxPrimeNumbers(String... args) {
		int range = Integer.parseInt(args[0]);
		List<Integer> list = primeNumbers.getPrimeNumbersInRange(range);
		assertThat(list.get(list.size()-1))
	      .isLessThanOrEqualTo(range);
	}
	
	
	
	@Before 
	public void before() {
		primeNumbers = new PrimeNumber();
	}
}
